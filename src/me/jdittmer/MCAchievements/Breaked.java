package me.jdittmer.MCAchievements;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Breaked {
    public static void Item(Material MaterialName, Player Player){
        Player player = (Player) Player;
        String playerName = player.getName();

        YamlConfiguration playerData = PlayerDataController.playerDataMap.get(playerName);

        // BREAK "LOG"
        if(MaterialName == Material.LOG){
            if(playerData.getInt("Achievements.BREAK_LOG.completed") == 1){
            	PlayerDataController.editPlayerData(playerName, "Achievements.BREAK_LOG.completed", 2);
            	PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WOOD.completed", 1);
                
                AchievementMessage.Completed(player, playerData.getString("Achievements.BREAK_LOG.name"), playerData.getString("Achievements.BREAK_LOG.description"));
                AchievementMessage.Unlock(player, playerData.getString("Achievements.CRAFT_WOOD.name"), playerData.getString("Achievements.CRAFT_WOOD.description"));
            }
        }
    }
}