package me.jdittmer.MCAchievements;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Basti on 01.06.2017.
 */
public class PlayerDataController {

    public static HashMap<String, YamlConfiguration> playerDataMap;
    public static File userdata;

    public static void initialize() {
        playerDataMap = new HashMap<String, YamlConfiguration>();
        userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("MCAchievements").getDataFolder(), File.separator + "UserData");
        userdata.mkdirs();
    }

    public static void initializePlayerData(String playerName) {
        File f = new File(userdata, File.separator + playerName + ".yml");

        if (!f.exists()) {
            System.out.println("File for player " + playerName + " doens't exist, creating new File");
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                YamlConfiguration playerData = new YamlConfiguration();
                // Here you can set what you want to be in the player file by default
                playerData.set("Achievements.BREAK_LOG.name", "Zerst�re den Wald");
                playerData.set("Achievements.BREAK_LOG.description", "Bringe einen Baum mit deiner blo�en Hand zum Umfallen!");
                playerData.set("Achievements.BREAK_LOG.completed", 1);

                playerData.set("Achievements.CRAFT_WOOD.name", "Schicker Boden");
                playerData.set("Achievements.CRAFT_WOOD.description", "Zerkloppe Baumst�mme vom zerst�rten Wald um Laminatboden herstellen zu k�nnen.");
                playerData.set("Achievements.CRAFT_WOOD.completed", 0);

                playerData.set("Achievements.CRAFT_WOODEN_BUTTON.name", "Knopf gebaut");
                playerData.set("Achievements.CRAFT_WOODEN_BUTTON.description", "Stelle einen Knopf lauter Holzsplitter *AUA* her.");
                playerData.set("Achievements.CRAFT_WOODEN_BUTTON.completed", 0);

                playerData.set("Achievements.CRAFT_WORKBENCH.name", "Endlich eine Werkbank");
                playerData.set("Achievements.CRAFT_WORKBENCH.description", "Baue eine Werkbank um so einiges mehr herstellen zu k�nnen.");
                playerData.set("Achievements.CRAFT_WORKBENCH.completed", 0);
                playerData.save(f);

                playerDataMap.put(playerName, playerData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            playerDataMap.put(playerName, YamlConfiguration.loadConfiguration(f));
        }
    }

    public static void editPlayerData(String playerName, String field, int value) {
        File f = new File(userdata, File.separator + playerName + ".yml");

        if(!f.exists()) {
            System.out.println("Unexpected error in editPlayerData, file doesn't exist!");
        }

        try {
            YamlConfiguration playerData = YamlConfiguration.loadConfiguration(f);
            playerData.set(field, value);
            playerData.save(f);

            playerDataMap.put(playerName, playerData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void editPlayerData(String playerName,String field, String value) {
        File f = new File(userdata, File.separator + playerName + ".yml");

        if(!f.exists()) {
            System.out.println("Unexpected error in editPlayerData, file doesn't exist!");
        }

        try {
            YamlConfiguration playerData = YamlConfiguration.loadConfiguration(f);
            playerData.set(field, value);
            playerData.save(f);

            playerDataMap.put(playerName, playerData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static YamlConfiguration getPlayerData(String playerName) {
        return playerDataMap.get(playerName);
    }

}