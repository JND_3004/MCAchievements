package me.jdittmer.MCAchievements;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.jdittmer.MCAchievements.Placed;

public class Placed {
	public static void Item(Material MaterialName, Player Player){
		Player player = (Player) Player;
		String playerName = player.getName();
		
		YamlConfiguration playerData = PlayerDataController.playerDataMap.get(playerName);
		
		if(playerData.getInt("Achievements.BREAK_LOG.completed") == 1 &&
				playerData.getInt("Achievements.CRAFT_WOOD.completed") == 0){
			PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WOOD.completed", 1);
			
			player.sendMessage(MCAchievements.plgChatSide() + "Du hast das Achievement " + ChatColor.GREEN + playerData.getString("Achievements.CRAFT_WOOD.name") + ChatColor.RESET + " erhalten!");
		}
	}
}