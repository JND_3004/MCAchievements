package me.jdittmer.MCAchievements;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import me.jdittmer.MCAchievements.JSONMessage;

public class AchievementMessage {
	public static void Completed(Player player, String name, String description) {
		player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
		
		JSONMessage.create("\n" + MCAchievements.plgChatSide() + "Du hast das Achievement ")
    	.then(name)
    		.color(ChatColor.GREEN)
    		.tooltip(JSONMessage.create(description)
    			.color(ChatColor.GRAY)
    		)
    	.then(" erhalten!\n")
    		.color(ChatColor.WHITE)
    	.send(player);
	}
	
	public static void Unlock(Player player, String name, String description) {
		JSONMessage.create(MCAchievements.plgChatSide())
    	.then(name)
    		.color(ChatColor.GREEN)
    		.tooltip(JSONMessage.create(description)
    			.color(ChatColor.GRAY)
    		)
    	.then(" freigeschaltet!")
    		.color(ChatColor.WHITE)
    	.send(player);
	}
}
