package me.jdittmer.MCAchievements;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Crafted {
	public static void Item(Material MaterialName, Player Player){
		Player player = (Player) Player;
		String playerName = player.getName();
		
		YamlConfiguration playerData = PlayerDataController.playerDataMap.get(playerName);
		
		// CRAFT "WOOD"
		if(MaterialName == Material.WOOD){
			if(playerData.getInt("Achievements.BREAK_LOG.completed") == 2 &&
					playerData.getInt("Achievements.CRAFT_WOOD.completed") == 1){
				PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WOOD.completed", 2);
				PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WORKBENCH.completed", 1);
				PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WOODEN_BUTTON.completed", 1);
				
				AchievementMessage.Completed(player, playerData.getString("Achievements.CRAFT_WOOD.name"), playerData.getString("Achievements.CRAFT_WOOD.description"));
				AchievementMessage.Unlock(player, playerData.getString("Achievements.CRAFT_WORKBENCH.name"), playerData.getString("Achievements.CRAFT_WORKBENCH.description"));
				AchievementMessage.Unlock(player, playerData.getString("Achievements.CRAFT_WOODEN_BUTTON.name"), playerData.getString("Achievements.CRAFT_WOODEN_BUTTON.description"));
			}
		}
		
		// CRAFT "WOODEN_BUTTON"
		if(MaterialName == Material.WOOD_BUTTON){
			if(playerData.getInt("Achievements.CRAFT_WOOD.completed") == 2 &&
					playerData.getInt("Achievements.CRAFT_WOODEN_BUTTON.completed") == 1){
				PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WOODEN_BUTTON.completed", 2);
				
				AchievementMessage.Completed(player, playerData.getString("Achievements.CRAFT_WOODEN_BUTTON.name"), playerData.getString("Achievements.CRAFT_WOODEN_BUTTON.description"));
			}
		}
		
		// CRAFT "WORKBENCH"
		if(MaterialName == Material.WORKBENCH){
			if(playerData.getInt("Achievements.CRAFT_WOOD.completed") == 2 &&
					playerData.getInt("Achievements.CRAFT_WORKBENCH.completed") == 1){
				PlayerDataController.editPlayerData(playerName, "Achievements.CRAFT_WORKBENCH.completed", 2);
				AchievementMessage.Completed(player, playerData.getString("Achievements.CRAFT_WORKBENCH.name"), playerData.getString("Achievements.CRAFT_WORKBENCH.description"));
			}
		}
	}
}