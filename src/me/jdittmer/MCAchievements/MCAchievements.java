package me.jdittmer.MCAchievements;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class MCAchievements extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        PlayerDataController.initialize();
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerJoin(PlayerJoinEvent event) {
        String playerName = event.getPlayer().getName();
        PlayerDataController.initializePlayerData(playerName);
    }

    public static String plgChatStart(){
        String title = ChatColor.GOLD + "-------------------- " + ChatColor.BOLD + "[MCAchievements]" + ChatColor.BOLD + " --------------------";
        return title;
    }

    public static String plgChatEnd(){
        String end = ChatColor.GOLD + "-------------------------------------------------------------";
        return end;
    }

    public static String plgChatSide(){
        String side = ChatColor.GOLD + "[MCAV] " + ChatColor.RESET;
        return side;
    }


    // CRAFTED
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void crafted(CraftItemEvent craftitem){
        Material crafted_item = craftitem.getRecipe().getResult().getType();
        Player player = (Player) craftitem.getWhoClicked();

        Crafted.Item(crafted_item, player);
    }


    // PLACED
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void placed(BlockPlaceEvent blockplace){
        Material placed_item = blockplace.getBlock().getType();
        Player player = (Player) blockplace.getPlayer();

        //Placed.Item(placed_item, player);
    }


    // BREAKED
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void breaked(BlockBreakEvent blockbreak){
        Material breaked_item = blockbreak.getBlock().getType();
        Player player = (Player) blockbreak.getPlayer();

        Breaked.Item(breaked_item, player);
    }


    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("mcav")) {
            if(args.length == 0){
                sender.sendMessage(plgChatStart());
                sender.sendMessage(ChatColor.GOLD +""+ ChatColor.BOLD + "Befehle von MCAchievements");
                if(sender.hasPermission("mcav.ver")){sender.sendMessage(ChatColor.GOLD + "/mcav ver" + ChatColor.RESET + ": Zeigt die Version des Plugins an.");}
                if(sender.hasPermission("mcav.mystats")){sender.sendMessage(ChatColor.GOLD + "/mcav mystats" + ChatColor.RESET + ": Zeigt deine Statistiken von MCAchievements an.");}
                if(sender.hasPermission("mcav.gui")){sender.sendMessage(ChatColor.GOLD + "/mcav gui" + ChatColor.RESET + ": �ffnet deine GUI f�r die Achievements.");}
                sender.sendMessage(plgChatEnd());
                return true;
            }else{
                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("ver")) {
                        if(sender.hasPermission("mcav.ver")){
                            sender.sendMessage(plgChatStart());
                            sender.sendMessage(ChatColor.GOLD +""+ ChatColor.BOLD + "MCAchievements Version:");
                            sender.sendMessage("Version: " + this.getDescription().getVersion());
                            sender.sendMessage("Von JND_3004 und bababababa");
                            sender.sendMessage(""+ChatColor.AQUA + ChatColor.UNDERLINE + "http://mc.jdittmer.com/");
                            sender.sendMessage(plgChatEnd());
                            return true;
                        }else{
                            sender.sendMessage(ChatColor.GOLD + "[MCAchievements] " + ChatColor.RED + "Du hast nicht gen�gend Rechte f�r diese Aktion!");
                            return true;
                        }
                    }
                }

                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("test")) {
                        sender.sendMessage("Hi " + ChatColor.BOLD + sender.getName() + ChatColor.RESET + ", this is the " + ChatColor.BOLD + "Version " + this.getDescription().getVersion() + ChatColor.RESET + " of " + ChatColor.BOLD + ChatColor.GOLD + this.getName() + ChatColor.RESET + " :)");
                        return true;
                    }
                }

                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("mystats")){
                        if(sender.hasPermission("mcav.mystats")){
                            if(args.length == 1){
                                MyStats.open_completed_achievements(sender);

                                return true;
                            }
                        }else{
                            sender.sendMessage(ChatColor.GOLD + "[MCAchievements] " + ChatColor.RED + "Du hast nicht gen�gend Rechte f�r diese Aktion!");
                            return true;
                        }
                    }

                    if(args[0].equalsIgnoreCase("gui")){
                        if(sender.hasPermission("mcav.gui")){
                            if(args.length == 1){
                                sender.sendMessage(plgChatStart());
                                sender.sendMessage(ChatColor.YELLOW + "*** Bald erscheint hier eine GUI! ***");
                                sender.sendMessage(plgChatEnd());

                                return true;
                            }
                        }else{
                            sender.sendMessage(ChatColor.GOLD + "[MCAchievements] " + ChatColor.RED + "Du hast nicht gen�gend Rechte f�r diese Aktion!");
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}